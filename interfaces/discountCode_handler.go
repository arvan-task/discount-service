package interfaces

import (
	"arvan_discount_service/application"
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/event"
	"arvan_discount_service/infrastructure/orderedid"
	"arvan_discount_service/infrastructure/orderedset"
	"arvan_discount_service/infrastructure/pipeline"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/nyaruka/phonenumbers"
	"net/http"
	"strings"
	"time"
)

type DiscountCodes struct {
	discountApp application.DiscountCodeAppInterface
	pipeline    pipeline.PipelineInterface
	orderedSet  orderedset.OrderedSetInterface
	orderedID   orderedid.OrderedIDServiceInterface
}

func NewDiscountCode(discountApp application.DiscountCodeAppInterface,
	pipeline pipeline.PipelineInterface,
	orderedSet orderedset.OrderedSetInterface,
	orderedID orderedid.OrderedIDServiceInterface) *DiscountCodes {
	return &DiscountCodes{
		discountApp: discountApp,
		pipeline:    pipeline,
		orderedSet:  orderedSet,
		orderedID:   orderedID,
	}
}

type requestCreateDiscountCode struct {
	Code         string  `json:"code" form:"code" binding:"required"`
	Count        uint32  `json:"count" form:"count" binding:"required,number"`
	ChargeAmount float64 `json:"charge_amount" form:"charge_amount" binding:"required,number"`
	ApplyDate    int32   `json:"apply_date" form:"apply_date" binding:"required,number"`
	ExpireDate   int32   `json:"expire_date" form:"expire_date" binding:"required,number"`
}

func createDiscountCodeValidation(err error) Meta {
	var metaErrors []MetaError
	switch err.(type) {
	case validator.ValidationErrors:
		errors := err.(validator.ValidationErrors)
		for _, validationError := range errors {
			switch validationError.StructField() {
			case "Code":
				metaErrors = append(metaErrors, MetaError{ErrorField: "phone_number",
					ErrorType: emptyPhoneNumber,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyPhoneNumber})
			case "Count":
				metaErrors = append(metaErrors, MetaError{ErrorField: "count",
					ErrorType: emptyCount,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyCount})
			case "ApplyDate":
				metaErrors = append(metaErrors, MetaError{ErrorField: "apply_date",
					ErrorType: emptyApplyDate,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyApplyDate})
			case "ExpireDate":
				metaErrors = append(metaErrors, MetaError{ErrorField: "expire_date",
					ErrorType: emptyExpireDate,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyExpireDate})
			case "ChargeAmount":
				metaErrors = append(metaErrors, MetaError{ErrorField: "charge_amount",
					ErrorType: emptyChargeAmount,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyChargeAmount})

			}
		}
	}
	return MetaErrors{Errors: metaErrors}
}

func (d *DiscountCodes) CreateDiscountCode(c *gin.Context) {
	var requestBody requestCreateDiscountCode

	err := c.ShouldBind(&requestBody)
	if err != nil {
		print(err.Error())
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: createDiscountCodeValidation(err)})
		return
	}

	var newDiscountCode entity.DiscountCode
	newDiscountCode.Code = requestBody.Code
	newDiscountCode.Count = requestBody.Count
	newDiscountCode.ChargeAmount = requestBody.ChargeAmount
	newDiscountCode.ApplyDate = time.Unix(int64(requestBody.ApplyDate), 0)
	newDiscountCode.ExpireDate = time.Unix(int64(requestBody.ExpireDate), 0)
	createdDiscountCode, err := d.discountApp.CreateDiscountCode(&newDiscountCode)
	if err != nil {
		//todo error logging
		c.JSON(http.StatusInternalServerError, nil)
		return
	}

	c.JSON(http.StatusCreated, BaseResponse{Data: createdDiscountCode, Meta: MetaResponse{StatusCode: http.StatusCreated, Message: discountCodeCreatedSuccessfully}})

}

type requestSubmitDiscountCode struct {
	PhoneNumber string `json:"phone_number" form:"phone_number" binding:"required"`
	Code        string `json:"code" form:"code" binding:"required"`
}

func submitDiscountCodeValidation(err error) Meta {
	var metaErrors []MetaError
	switch err.(type) {
	case validator.ValidationErrors:
		errors := err.(validator.ValidationErrors)
		for _, validationError := range errors {
			switch validationError.StructField() {
			case "PhoneNumber":
				metaErrors = append(metaErrors, MetaError{ErrorField: "phone_number",
					ErrorType: emptyPhoneNumber,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyPhoneNumber})
			case "Code":
				metaErrors = append(metaErrors, MetaError{ErrorField: "code",
					ErrorType: emptyDiscountCode,
					ErrorCode: http.StatusUnprocessableEntity,
					Message:   emptyDiscountCode})

			}
		}
	}
	return MetaErrors{Errors: metaErrors}
}

func (d *DiscountCodes) SubmitDiscountCode(c *gin.Context) {
	var requestBody requestSubmitDiscountCode

	err := c.ShouldBind(&requestBody)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: submitDiscountCodeValidation(err)})
		return
	}

	if !strings.HasPrefix(requestBody.PhoneNumber, "+") {
		requestBody.PhoneNumber = "+" + requestBody.PhoneNumber
	}

	phone, err := phonenumbers.Parse(requestBody.PhoneNumber, "")
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorField: "phone_number",
			ErrorType:  invalidPhoneNumber,
			ErrorCode:  http.StatusUnprocessableEntity,
			Message:    invalidPhoneNumber}}}})
		return
	}
	isValidNumber := phonenumbers.IsValidNumber(phone)

	if !isValidNumber {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorField: "phone_number",
			ErrorType:  invalidPhoneNumber,
			ErrorCode:  http.StatusUnprocessableEntity,
			Message:    invalidPhoneNumber}}}})
		return
	}

	var discountCodeSubmittedEvent event.DiscountCodeSubmitted
	discountCodeSubmittedEvent.PhoneNumber = requestBody.PhoneNumber
	discountCodeSubmittedEvent.Code = requestBody.Code
	discountCodeSubmittedEvent.OrderID = d.orderedID.GenerateOrderedID()

	eventPayload, err := json.Marshal(&discountCodeSubmittedEvent)
	if err != nil {
		//todo error logging
		c.JSON(http.StatusInternalServerError, nil)
		return
	}
	err = d.pipeline.ProduceMessage(eventPayload)
	if err != nil {
		err = d.orderedSet.Push(eventPayload, discountCodeSubmittedEvent.OrderID)
		if err != nil {
			//todo error logging
			c.JSON(http.StatusInternalServerError, nil)
			return
		}
	}

	c.JSON(http.StatusAccepted, BaseResponse{Data: nil, Meta: MetaResponse{StatusCode: http.StatusAccepted, Message: discountCodeSubmittedSuccessfully}})

}
