package interfaces

import (
	"arvan_discount_service/application"
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/event"
	"arvan_discount_service/infrastructure/chargeservice"
	"arvan_discount_service/infrastructure/orderedset"
	"encoding/json"
)

type DiscountCodeSubmission struct {
	discountApp        application.DiscountCodeAppInterface
	discountRequestApp application.DiscountRequestAppInterface
	OrderedSet         orderedset.OrderedSetInterface
	ChargeService      chargeservice.ChargeServiceInterface
}

func NewDiscountCodeSubmission(orderedSet orderedset.OrderedSetInterface,
	chargeService chargeservice.ChargeServiceInterface,
	discountApp application.DiscountCodeAppInterface,
	discountRequestApp application.DiscountRequestAppInterface) *DiscountCodeSubmission {
	return &DiscountCodeSubmission{OrderedSet: orderedSet,
		ChargeService:      chargeService,
		discountRequestApp: discountRequestApp,
		discountApp:        discountApp}
}

func (d *DiscountCodeSubmission) SubmitDiscountCode(payload []byte) {
	var discountCodeSubmittedEvent event.DiscountCodeSubmitted

	err := json.Unmarshal(payload, &discountCodeSubmittedEvent)

	if err != nil {
		//todo error logging
		return
	}

	discountCode, err := d.discountApp.GetDiscountCode(discountCodeSubmittedEvent.Code)
	if err != nil {
		//todo error logging
		return
	}

	discountRequestsCount, err := d.discountRequestApp.CountDiscountCodeRequests(discountCode.Code)

	minOrderedSetElement, err := d.OrderedSet.GetMin()
	if err != nil || minOrderedSetElement == nil {
		err = d.discountRequestApp.SaveDiscountRequest(&entity.DiscountRequest{Code: discountCode.Code,
			PhoneNumber: discountCodeSubmittedEvent.PhoneNumber})
		if err != nil {
			//todo error logging
			return
		}
		if discountRequestsCount < int64(discountCode.Count) {
			err = d.ChargeService.ChargeAccount(discountCodeSubmittedEvent.PhoneNumber, discountCode.ChargeAmount)
			if err != nil {
				//todo error logging
				return
			}
		}

	} else {
		var discountRequestOrderedSetEvent event.DiscountCodeSubmitted
		err := json.Unmarshal(minOrderedSetElement, &discountRequestOrderedSetEvent)
		if err != nil {
			//todo error logging
			return
		}

		if discountRequestOrderedSetEvent.OrderID < discountCodeSubmittedEvent.OrderID && discountRequestOrderedSetEvent.PhoneNumber == discountCodeSubmittedEvent.PhoneNumber {
			err = d.discountRequestApp.SaveDiscountRequest(&entity.DiscountRequest{Code: discountCode.Code, PhoneNumber: discountRequestOrderedSetEvent.PhoneNumber})
			if err != nil {
				//todo error logging
				return
			}
			if discountRequestsCount >= int64(discountCode.Count) {
				err = d.ChargeService.ChargeAccount(discountRequestOrderedSetEvent.PhoneNumber, discountCode.ChargeAmount)
				if err != nil {
					//todo error logging
					return
				}
			}

		} else {
			err = d.discountRequestApp.SaveDiscountRequest(&entity.DiscountRequest{Code: discountCode.Code, PhoneNumber: discountCodeSubmittedEvent.PhoneNumber})
			if err != nil {
				//todo error logging
				return
			}
			if discountRequestsCount >= int64(discountCode.Count) {
				err = d.ChargeService.ChargeAccount(discountCodeSubmittedEvent.PhoneNumber, discountCode.ChargeAmount)
				if err != nil {
					//todo error logging
					return
				}
			}

		}

	}

}
