package interfaces

type BaseResponse struct {
	Data Data `json:"data,omitempty"`
	Meta Meta `json:"meta"`
}

type MetaError struct {
	ErrorField string `json:"error_field,omitempty"`
	ErrorType  string `json:"error_type"`
	ErrorCode  int    `json:"error_code"`
	Message    string `json:"message"`
}

type PaginationMeta struct {
	Offset    int `json:"offset"`
	Limit     int `json:"limit"`
	PageSize  int `json:"page_size"`
	TotalSize int `json:"total_size"`
}

type MetaResponse struct {
	StatusCode int             `json:"status_code,omitempty"`
	Message    string          `json:"message,omitempty"`
	Pagination *PaginationMeta `json:"pagination,omitempty"`
}

type MetaErrors struct {
	Errors []MetaError `json:"errors"`
}

type Meta interface {
}

type Data interface {
}
