package interfaces

const (
	invalidPhoneNumber = "INVALID_PHONE_NUMBER"
	emptyPhoneNumber   = "EMPTY_PHONE_NUMBER"
	emptyDiscountCode  = "EMPTY_DISCOUNT_CODE"
	emptyCount         = "EMPTY_COUNT"
	invalidCount       = "EMPTY_COUNT"
	emptyApplyDate     = "EMPTY_APPLY_DATE"
	invalidApplyDate   = "EMPTY_APPLY_DATE"
	emptyExpireDate    = "EMPTY_EXPIRE_DATE"
	emptyChargeAmount    = "EMPTY_CHARGE_AMOUNT"
	invalidExpireDate  = "INVALID_EXPIRE_DATE"
)
