package repository

import (
	"arvan_discount_service/domain/entity"
	"errors"
)

type DiscountRequestRepository interface {
	SaveDiscountRequest(request *entity.DiscountRequest)error
	CountDiscountCodeRequests(code string)(int64,error)
}


var (
	ErrDupDiscountRequest = errors.New("duplicate discount request")
)