package repository

import (
	"arvan_discount_service/domain/entity"
	"errors"
)

type DiscountCodeRepository interface {
	CreateDiscountCode(code *entity.DiscountCode) (*entity.DiscountCode, error)
	GetDiscountCode(code string) (*entity.DiscountCode, error)
}

var (
	ErrDiscountCodeNotFound = errors.New("discount not found")
	ErrDupDiscountCode = errors.New("duplicate discount code")
)