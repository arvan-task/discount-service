package entity

import "time"

type DiscountCode struct {
	ID           uint32    `json:"id"`
	Code         string    `json:"code" gorm:"unique;not null"`
	Count        uint32    `json:"count" gorm:"not null"`
	ChargeAmount float64   `json:"charge_amount" gorm:"not null"`
	ApplyDate    time.Time `json:"apply_date" gorm:"not null"`
	ExpireDate   time.Time `json:"expire_date" gorm:"not null"`
	CreatedAt    time.Time `json:"created_at"`
}
