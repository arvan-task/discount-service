package entity

type DiscountRequest struct {
	ID          int64  `json:"id"`
	PhoneNumber string `json:"phone_number" gorm:"index:idx_request_phone_code,unique;size:15;not null"`
	Code        string `json:"code" gorm:"index:idx_request_phone_code,unique;not null"`
}
