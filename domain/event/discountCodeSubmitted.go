package event

type DiscountCodeSubmitted struct {
	OrderID     int64  `json:"order_id"`
	PhoneNumber string `json:"phone_number"`
	Code        string `json:"code"`
}
