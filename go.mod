module arvan_discount_service

go 1.14

require (
	github.com/Shopify/sarama v1.27.2
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis/v8 v8.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/nyaruka/phonenumbers v1.0.59
	github.com/valyala/fasthttp v1.17.0
)
