package application

import (
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/repository"
)


type discountRequestApp struct {
	discountRequestRepository repository.DiscountRequestRepository
}

func (d discountRequestApp) SaveDiscountRequest(request *entity.DiscountRequest) error {
	return d.discountRequestRepository.SaveDiscountRequest(request)
}

func (d discountRequestApp) CountDiscountCodeRequests(code string) (int64, error) {
	return d.discountRequestRepository.CountDiscountCodeRequests(code)
}

var _ DiscountRequestAppInterface = &discountRequestApp{}



type DiscountRequestAppInterface interface {
	SaveDiscountRequest(request *entity.DiscountRequest)error
	CountDiscountCodeRequests(code string)(int64,error)
}
