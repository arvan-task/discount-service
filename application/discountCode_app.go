package application

import (
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/repository"
)

type discountCodeApp struct {
	discountRepository repository.DiscountCodeRepository
}

func (d discountCodeApp) CreateDiscountCode(code *entity.DiscountCode) (*entity.DiscountCode, error) {
	return d.discountRepository.CreateDiscountCode(code)
}

func (d discountCodeApp) GetDiscountCode(code string) (*entity.DiscountCode, error) {
	return d.discountRepository.GetDiscountCode(code)
}

var _ DiscountCodeAppInterface = &discountCodeApp{}

type DiscountCodeAppInterface interface {
	CreateDiscountCode(code *entity.DiscountCode) (*entity.DiscountCode, error)
	GetDiscountCode(code string) (*entity.DiscountCode, error)
}

