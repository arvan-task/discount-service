package main

import (
	"arvan_discount_service/infrastructure/chargeservice"
	"arvan_discount_service/infrastructure/orderedid"
	"arvan_discount_service/infrastructure/orderedset"
	"arvan_discount_service/infrastructure/persistence"
	"arvan_discount_service/infrastructure/pipeline"
	"arvan_discount_service/interfaces"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

func main() {

	config, err := LoadConfiguration(os.Args[1:])
	if err != nil {
		panic(err)
	}

	services, err := persistence.NewRepositories(config.Database.Driver,
		config.Database.Username,
		config.Database.Password,
		config.Database.Port,
		config.Database.Host,
		config.Database.Database)
	if err != nil {
		panic(err)
	}
	defer services.Close()
	services.AutoMigrate()

	orderedSetService, err := orderedset.NewRedis(config.Redis.Address,
		config.Redis.Database,
		config.Redis.Username,
		config.Redis.Password)
	if err != nil {
		panic(err)
	}

	chargeService := chargeservice.NewChargeAPIClient(config.ChargeService.BaseURL, config.ChargeService.Timeout)

	if chargeService.ChargeService == nil {
		panic("null")
	}
	discountCodeSubmission := interfaces.NewDiscountCodeSubmission(orderedSetService.OrderedSet,
		chargeService.ChargeService,
		services.DiscountCode,
		services.DiscountRequest)

	pipelineService, err := pipeline.NewKafkaClient(config.Kafka.Address,
		config.Kafka.Group,
		config.Kafka.DiscountCodeRequestsTopic, discountCodeSubmission.SubmitDiscountCode)
	if err != nil {
		panic(err)
	}
	go pipelineService.KafkaClient.Start()

	orderedIDService, err := orderedid.NewSnowflakeNode(int64(config.NodeNumber))
	if err != nil {
		panic(err)
	}
	discounts := interfaces.NewDiscountCode(services.DiscountCode,
		pipelineService.Pipeline,
		orderedSetService.OrderedSet,
		orderedIDService.OrderedID)

	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	v1 := router.Group("v1")
	{
		discountCodeGroup := v1.Group("/discount-codes")
		{
			discountCodeGroup.POST("", discounts.CreateDiscountCode)
			discountCodeGroup.POST("/submission", discounts.SubmitDiscountCode)
		}
	}

	log.Fatal(router.Run(config.HttpServer.Address))
}
