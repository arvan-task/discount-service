package orderedset

import "github.com/go-redis/redis/v8"

type RedisService struct {
	OrderedSet OrderedSetInterface
	Client     *redis.Client
}

const redisOrderedSetKey = "requestsSet"

func NewRedis(address string, database int, username string, password string) (*RedisService, error) {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: password,
		DB:       database,
		Username: username,
	})
	return &RedisService{
		OrderedSet: NewOrderedSet(redisClient),
		Client:     redisClient,
	}, nil
}
