package orderedset

import (
	"context"
	"github.com/go-redis/redis/v8"
)

type OrderedSetInterface interface {
	Push(payload []byte, score int64) error
	PopMin() ([]byte, error)
	PopMax() ([]byte, error)
	GetMin() ([]byte, error)
	GetMax() ([]byte, error)
}

type OrderedSetClient struct {
	client *redis.Client
}

func (o OrderedSetClient) Push(payload []byte, score int64) error {
	return o.client.ZAdd(context.Background(),redisOrderedSetKey,&redis.Z{Member: payload,Score: float64(score)}).Err()

}

func (o OrderedSetClient) PopMin() ([]byte, error) {
	resultSet , err:=o.client.ZPopMin(context.Background() ,redisOrderedSetKey ).Result()
	if err!=nil{
		return nil, err
	}
	if len(resultSet)>0{
		return resultSet[0].Member.([]byte),nil
	}else{
		return nil, nil
	}
}

func (o OrderedSetClient) PopMax() ([]byte, error) {
	resultSet , err:=o.client.ZPopMax(context.Background() ,redisOrderedSetKey ).Result()
	if err!=nil{
		return nil, err
	}
	if len(resultSet)>0{
		return resultSet[0].Member.([]byte),nil
	}else{
		return nil, nil
	}
}

func (o OrderedSetClient) GetMin() ([]byte, error) {
	resultSet , err:=o.client.ZRangeWithScores(context.Background(),redisOrderedSetKey,0,0).Result()
	if err!=nil{
		return nil, err
	}
	if len(resultSet)>0{
		return resultSet[0].Member.([]byte),nil
	}else{
		return nil, nil
	}

}

func (o OrderedSetClient) GetMax() ([]byte, error) {
	resultSet , err:=o.client.ZRevRangeWithScores(context.Background(),redisOrderedSetKey,0,0).Result()
	if err!=nil{
		return nil, err
	}
	if len(resultSet)>0{
		return resultSet[0].Member.([]byte),nil
	}else{
		return nil, nil
	}
}

var _ OrderedSetInterface = &OrderedSetClient{}

func NewOrderedSet(client *redis.Client) *OrderedSetClient {
	return &OrderedSetClient{client: client}
}
