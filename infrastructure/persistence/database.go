package persistence

import (
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/repository"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Repositories struct {
	DiscountCode repository.DiscountCodeRepository
	DiscountRequest repository.DiscountRequestRepository
	db           *gorm.DB
}

func NewRepositories(DbDriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*Repositories, error) {
	databaseConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", DbUser, DbPassword, DbHost, DbPort, DbName)
	db, err := gorm.Open(DbDriver, databaseConnectionString)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)

	return &Repositories{
		DiscountCode: NewDiscountCodeRepository(db),
		DiscountRequest: NewDiscountRequestRepository(db),
		db:           db,
	}, nil
}

//closes the  database connection
func (s *Repositories) Close() error {
	return s.db.Close()
}

//This migrate all tables
func (s *Repositories) AutoMigrate() error {
	return s.db.AutoMigrate(&entity.DiscountCode{},&entity.DiscountRequest{}).Error
}
