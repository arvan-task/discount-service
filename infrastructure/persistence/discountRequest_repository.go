package persistence

import (
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/repository"
	"github.com/jinzhu/gorm"
)

type DiscountRequest struct {
	db *gorm.DB
}

func (d DiscountRequest) SaveDiscountRequest(request *entity.DiscountRequest) error {
	err := d.db.Create(&request).Error
	if err!=nil{
		return err
	}
	return nil
}

func (d DiscountRequest) CountDiscountCodeRequests(code string) (int64, error) {
	var rowCount int64
	err:=d.db.Model(entity.DiscountRequest{}).Where("code=?",code).Count(&rowCount).Error
	if err!=nil{
		return 0, err
	}
	return rowCount,nil
}

func NewDiscountRequestRepository(db *gorm.DB) *DiscountRequest {
	return &DiscountRequest{db}
}

var _ repository.DiscountRequestRepository = &DiscountRequest{}
