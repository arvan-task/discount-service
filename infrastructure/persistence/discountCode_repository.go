package persistence

import (
	"arvan_discount_service/domain/entity"
	"arvan_discount_service/domain/repository"
	"github.com/jinzhu/gorm"
)

type DiscountCode struct {
	db *gorm.DB
}

func (d DiscountCode) CreateDiscountCode(code *entity.DiscountCode) (*entity.DiscountCode, error) {
	err := d.db.Create(&code).Error
	if err!=nil{
		return nil, err
	}

	return code, nil
}

func (d DiscountCode) GetDiscountCode(code string) (*entity.DiscountCode, error) {
	var discountCode entity.DiscountCode
	err := d.db.Model(discountCode).Find(&discountCode,"code=?",code).Error
	if err!=nil{
		return nil, err
	}
	return &discountCode, nil
}

func NewDiscountCodeRepository(db *gorm.DB) *DiscountCode {
	return &DiscountCode{db}
}

var _ repository.DiscountCodeRepository = &DiscountCode{}
