package pipeline

import (
	"context"
	"errors"
	"fmt"
	"github.com/Shopify/sarama"
	"time"
)

type KafkaService struct {
	Pipeline    PipelineInterface
	KafkaClient *KafkaClient
}
type KafkaClient struct {
	ConsumerHandler func(value []byte)
	ProducerClient  sarama.SyncProducer
	Config          struct {
		Address string
		Group   string
		Topic   string
	}
}

func NewKafkaClient(kafkaAddress string, kafkaGroup string, kafkaTopic string, consumerHandler func(value []byte)) (*KafkaService, error) {

	conf := sarama.NewConfig()
	conf.Version = sarama.V2_5_0_0
	conf.Producer.Return.Successes=true


	kafkaProducer, err := sarama.NewSyncProducer([]string{kafkaAddress}, conf)
	if err != nil {
		println(err.Error())
	}

	kafkaClient := &KafkaClient{ConsumerHandler: consumerHandler, ProducerClient: kafkaProducer, Config: struct {
		Address string
		Group   string
		Topic   string
	}{Address: kafkaAddress, Group: kafkaGroup, Topic: kafkaTopic}}

	return &KafkaService{KafkaClient: kafkaClient, Pipeline: Newpipeline(kafkaClient)}, nil

}

func (k *KafkaClient) Start() error {
	config := sarama.NewConfig()
	config.Version = sarama.V2_5_0_0
	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	group, err := sarama.NewConsumerGroup([]string{k.Config.Address}, k.Config.Group, config)
	if err != nil {
		return err
	}

	go func() {
		for err := range group.Errors() {
			panic(err)
		}
	}()

	func() {
		ctx := context.Background()
		for {
			topics := []string{k.Config.Topic}
			err := group.Consume(ctx, topics, k)
			if err != nil {
				fmt.Printf("kafka consume failed: %v, sleeping and retry in a moment\n", err)
				time.Sleep(time.Second)
			}
		}
	}()
	return nil
}

func (k *KafkaClient) Setup(_ sarama.ConsumerGroupSession) error {
	return nil
}

func (k *KafkaClient) Cleanup(_ sarama.ConsumerGroupSession) error {
	return nil
}

func (k *KafkaClient) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for msg := range claim.Messages() {
		sess.MarkMessage(msg, "")
		k.ConsumerHandler(msg.Value)
	}
	return nil
}

func (k *KafkaClient) produceMessage(payload []byte) error {
	if k.ProducerClient==nil{
		return errors.New("kafka producer is not ready")
	}
	msg := &sarama.ProducerMessage{
		Topic: k.Config.Topic,
		Value: sarama.StringEncoder(payload),
	}
	_, _, err := k.ProducerClient.SendMessage(msg)
	return err
}
