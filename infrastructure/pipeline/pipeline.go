package pipeline

type PipelineInterface interface {
	ProduceMessage(payload []byte) error
}

type PipelineClient struct {
	client *KafkaClient
}

func (p PipelineClient) ProduceMessage(payload []byte) error {
	return p.client.produceMessage(payload)
}

var _ PipelineInterface = &PipelineClient{}

func Newpipeline(kafkaClient *KafkaClient) *PipelineClient {
	return &PipelineClient{client: kafkaClient}
}
