package orderedid

type OrderedIDServiceInterface interface {
	GenerateOrderedID() int64
}

type OrderedIDClient struct {
	client *Node
}

func (o OrderedIDClient) GenerateOrderedID() int64 {
	return o.client.Generate().Int64()
}

var _ OrderedIDServiceInterface = &OrderedIDClient{}

func NewOrderedID(client *Node) *OrderedIDClient {
	return &OrderedIDClient{client: client}
}
