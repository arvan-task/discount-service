package chargeservice

import (
	"errors"
	"github.com/valyala/fasthttp"
	"strconv"
	"time"
)

type ChargeServiceInterface interface {
	ChargeAccount(phoneNumber string, amount float64) error
}

type ChargeAPIServiceClient struct {
	client *ChargeAPIClient
}

func (c ChargeAPIServiceClient) ChargeAccount(phoneNumber string, amount float64) error {
	httpClient := fasthttp.Client{MaxIdleConnDuration: time.Duration(c.client.Timeout) * time.Millisecond}
	args := fasthttp.AcquireArgs()
	defer fasthttp.ReleaseArgs(args)

	args.Add("change_amount", strconv.FormatFloat(amount, 'f', 6, 64))

	statusCode, _, err := httpClient.Post(nil, c.client.BaseURL+"/accounts/"+phoneNumber+"/balance/change", args)
	if err != nil {
		return err
	}

	if statusCode == fasthttp.StatusOK {
		return nil
	}
	return errors.New("failed to charge user account")
}

var _ ChargeServiceInterface = &ChargeAPIServiceClient{}

func NewChargeService(client *ChargeAPIClient) *ChargeAPIServiceClient {
	return &ChargeAPIServiceClient{client: client}
}
