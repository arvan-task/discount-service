package chargeservice

type ChargeAPIClient struct {
	BaseURL string
	Timeout int
}

type ChargeAPIService struct {
	ChargeService ChargeServiceInterface
	Client        *ChargeAPIClient
}

func NewChargeAPIClient(baseURL string,timeout int) ChargeAPIService {
	client :=&ChargeAPIClient{BaseURL: baseURL,Timeout: timeout}
	return ChargeAPIService{ChargeService:NewChargeService(client),Client: client}
}
